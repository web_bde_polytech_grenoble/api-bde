// INIT THE SERVER
var express = require('express');
var cors = require('cors');
var cron = require('node-cron');

app = express(); // create app
app.use(cors({origin: '*'})); // accept all request from all orgin (header of http)
port = process.env.PORT || 3000;

// ADD ROUTES
var routes = require('./src/app/routes/routes');
routes(app);

// SCHEDULE
extract = require('./src/app/controllers/agenda-controller');
// cron.schedule("*/1 * * * *", () => {
//     extract.extractingADE();
// });

// LAUNCH THE SERVER
app.listen(port);
console.log('grebdepolytechweb RESTful API server started on: ' + port);
