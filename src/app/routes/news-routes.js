'use strict';

var controller = require('../controllers/news-controller');

module.exports = function(app) {
    app.route('/news')
        .get(controller.getAllNews);
}