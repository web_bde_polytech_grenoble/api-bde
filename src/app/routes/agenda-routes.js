'use strict';

var controller = require('../controllers/agenda-controller');

module.exports = function(app) {
    app.route('/agenda/english')
        .get(controller.getIdsEnglish);

    app.route('/agenda/branchs')
        .get(controller.getIdsBranchs);

    app.route('/agenda/ids/:ids')
        .get(controller.getADE);

    app.route('/agenda/id/:id')
        .get(controller.getInfosId);
}