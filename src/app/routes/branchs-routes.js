'use strict';

var controller = require('../controllers/branchs-controller');

module.exports = function(app) {
    app.route('/branchs')
        .get(controller.getAllBranchs);
}