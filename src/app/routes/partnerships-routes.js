'use strict';

var controller = require('../controllers/partnerships-controller');

module.exports = function(app) {
    app.route('/partnerships')
        .get(controller.getAllPartnerships);
}