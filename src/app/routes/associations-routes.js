'use strict';

var controller = require('../controllers/associations-controller');

module.exports = function(app) {
    app.route('/associations')
        .get(controller.getAllAssociation);
}