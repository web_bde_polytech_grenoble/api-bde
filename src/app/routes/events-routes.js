'use strict';

var controller = require('../controllers/events-controller');

module.exports = function(app) {
    app.route('/events')
        .get(controller.getAllEvents);
}