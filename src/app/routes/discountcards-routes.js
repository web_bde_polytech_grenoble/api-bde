'use strict';

var controller = require('../controllers/discountcards-controller');

module.exports = function(app) {
    app.route('/discountcards')
        .get(controller.getAllDiscountcards);
}