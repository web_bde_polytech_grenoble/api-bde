'use strict';

var controller = require('../controllers/shortcuts-controller');

module.exports = function(app) {
    app.route('/shortcuts')
        .get(controller.getAllShortcuts);
}