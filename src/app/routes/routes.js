'use strict';

module.exports = function(app) {
    // add branchs routes
    var routes = require('./associations-routes');
    routes(app);

    // add branchs routes
    routes = require('./branchs-routes');
    routes(app);

    // add discountscards routes
    routes = require('./discountcards-routes');
    routes(app);

    // add events routes
    routes = require('./events-routes');
    routes(app);

    // add news routes
    routes = require('./news-routes');
    routes(app);

    // add partnerships routes
    routes = require('./partnerships-routes');
    routes(app);

    // add shortcuts routes
    routes = require('./shortcuts-routes');
    routes(app);

    // add songs routes
    routes = require('./songs-routes');
    routes(app);

    // add agenda routes
    routes = require('./agenda-routes');
    routes(app);
}