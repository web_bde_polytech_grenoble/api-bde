'use strict';

var controller = require('../controllers/songs-controller');

module.exports = function(app) {
    app.route('/songs')
        .get(controller.getAllSongs);
}