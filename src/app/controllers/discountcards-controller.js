'use strict';

const data = require('../../data/discountcards-data.json');

exports.getAllDiscountcards = function(req, res){
    res.json({
        'data' : data
    });
}