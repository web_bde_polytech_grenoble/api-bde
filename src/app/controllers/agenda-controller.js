'use strict';

const ical = require("node-ical");
const agendaIds = require("../../data/agenda_ids_2019_2020.json");
const ColorHash = require("color-hash");

function getDate() {
    var firstDate;
    var lastDate;

    var today = new Date();
    var datePivot = new Date(today.getFullYear()+"-08-01");
    if (today >= datePivot){
        firstDate = datePivot.getFullYear() + "-08-01";
        lastDate = (datePivot.getFullYear()+1) + "-08-01";
    }else{
        firstDate = (datePivot.getFullYear()-1)+"-08-01";
        lastDate = datePivot.getFullYear()+"-08-01";
    }

    return {
        "firstDate" : firstDate,
        "lastDate" : lastDate
    }
}

function jsonToEvent(json){
    var colorHash = new ColorHash();
    return Object.keys(json).map((key) => {
        const event = json[key];
        return {
          "description": event["description"],
          "location": event["location"],
          "start": new Date(event["start"]),
          "end": new Date(event["end"]),
          "id": key,
          "title": event["summary"] + '\n \n' + event["location"],
          "trueTitle": event["summary"],
          "color": colorHash.hex(event["summary"])
        };
    });
}

exports.extractingADE = async function (ids) {
    var dates = getDate();
    var url = "http://ade-sts.grenet.fr/jsp/custom/modules/plannings/anonymous_cal.jsp?resources="+ids+"&projectId=2&calType=ical&firstDate="+dates["firstDate"]+"&lastDate="+dates["lastDate"];
    var ret;
    
    // get the exported ADE from URL
    await ical.async.fromURL(url).then(
        (value) => {
            // convert the json to the true format (@fullcalendar)
            ret = jsonToEvent(value);
        }
    );

    return ret;
}

exports.getADE = async function(req, res){
    var ids = req.params.ids;
    var error = false;
    var ret;
    try{
        ret = await exports.extractingADE(ids);
    }catch(err){
        error = true;
    }
    res.json({
        'error' : error,
        'data' : ret
    });
}

function jsonToBranchInfo(json){
    return Object.keys(json).map((key) => {
        const field = json[key];
        return {
          "name": key,
          "id": field
        };
    });
}

function getIdsBranchs(){
    // allow to not modify the gloabl variable agendaIds -> copy it on other variable
    var tmp = JSON.parse(JSON.stringify(agendaIds));
    delete tmp["TRONC COMMUN 3ème ANNEE"];

    return Object.keys(tmp).map((key) => {
        const cat = tmp[key];
        var test = key;
        return {
            "branch" : key,
            "years" : Object.keys(cat).map((key2) => {
                const field = cat[key2];
                return {
                    "year" : key2,
                    "group" : jsonToBranchInfo(field)
                }
            })
        };
    });
}

exports.getIdsBranchs = function(req, res){
    res.json({
        'data' : getIdsBranchs()
    });
}

function getIdsEnglish(){
    var tmp = agendaIds["TRONC COMMUN 3ème ANNEE"]["Groupe TC3"];
    
    return jsonToBranchInfo(tmp);
} 

exports.getIdsEnglish = function(req, res){
    res.json({
        'data' : getIdsEnglish()
    });
}

exports.getInfosId = function(req, res){
    res.json({
        'data' : null,
        'error' : false
    });
}