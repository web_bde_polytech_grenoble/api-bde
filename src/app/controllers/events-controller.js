'use strict';

const data = require('../../data/events-data.json');

exports.getAllEvents = function(req, res){
    res.json({
        'data' : data
    });
}