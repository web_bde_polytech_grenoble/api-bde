'use strict';

const data = require('../../data/songs-data.json');

exports.getAllSongs = function(req, res){
    res.json({
        'data' : data
    });
}