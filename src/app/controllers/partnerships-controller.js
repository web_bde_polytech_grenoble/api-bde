'use strict';

const data = require('../../data/partnerships-data.json');

exports.getAllPartnerships = function(req, res){
    res.json({
        'data' : data
    });
}