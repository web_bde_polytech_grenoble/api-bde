'use strict';

const data = require('../../data/news-data.json');

exports.getAllNews = function(req, res){
    res.json({
        'data' : data
    });
}