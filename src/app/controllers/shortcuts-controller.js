'use strict';

const data = require('../../data/shortcuts-data.json');

exports.getAllShortcuts = function(req, res){
    res.json({
        'data' : data
    });
}